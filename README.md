The file "dplots.mw" contains the Maple code used to create Fig. 1 in our paper (ECC2024.pdf), and the text version of this code is available in "dplots.txt".
This code has been checked with Maple 2020.0, see https://www.maplesoft.com/.
